import React, { Component } from 'react';
import { Transition } from 'semantic-ui-react';
import CSSTransition from 'react-transition-group/CSSTransition';

import './style.scss';

export default class Fade extends Component {
  state = {
    visible: false,
  };
  componentDidMount() {
    // setTimeout(
    //   function() {
    //     this.setState({ visible: true });
    //   }.bind(this),
    //   10
    // );
    this.setState({ visible: true });
  }

  render() {
    return (
      <Transition visible={this.state.visible} animation={this.props.animation || 'fade in'} duration={this.props.duration || 500}>
        <div>{this.props.children}</div>
      </Transition>
    );
  }
}

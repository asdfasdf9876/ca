import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Scroll from 'react-scroll-to-element';
import { Menu, Header, Image, Grid } from 'semantic-ui-react';
import './style.scss';
import logo from './../../../assets/img/logo.png';

export default class Nav extends Component {
  state = {
    activeItem: '',
  };

  componentDidMount() {
    console.log(this.props.location);
    switch (this.props.location) {
      case '/':
        this.setState({ activeItem: 'group' });
        break;
      case '/brands':
        this.setState({ activeItem: 'brands' });
        break;
      case '/technology':
        this.setState({ activeItem: 'technology' });
        break;
      case '/talent':
        this.setState({ activeItem: 'talent' });
        break;
      case '/ethical_code':
        this.setState({ activeItem: 'ethical_code' });
        break;
      case '/investor_relations':
        this.setState({ activeItem: 'investor_relations' });
        break;

      default:
        this.setState({ activeItem: '' });
    }
  }

  render() {
    return (
      <div>
        <Menu className="main-menu" secondary fixed="top">
          <Grid className="nav-grid">
            <Grid.Row>
              <Menu className="sub-menu" pointing secondary>
                <Menu.Item>
                  <Image size="tiny" src={logo} />
                </Menu.Item>
                <Menu.Item className="first-item" name="group" active={this.state.activeItem === 'group'} onClick={this.handleItemClick}>
                  <Scroll type="id" element="group" offset={-100}>
                    <Link to="/"> Group </Link>
                  </Scroll>
                </Menu.Item>
                <Menu.Item name="talent" active={this.state.activeItem === 'talent'} onClick={this.handleItemClick}>
                  <Scroll type="id" element="talent" offset={-100}>
                    <Link to="/"> Talent</Link>
                  </Scroll>
                </Menu.Item>
                <Menu.Item name="technology" active={this.state.activeItem === 'technology'} onClick={this.handleItemClick}>
                  <Scroll type="id" element="technology" offset={-100}>
                    <Link to="/technology"> Technology</Link>
                  </Scroll>
                </Menu.Item>
                <Menu.Item name="brands" active={this.state.activeItem === 'brands'} onClick={this.handleItemClick}>
                  <Scroll type="id" element="brands" offset={-100}>
                    <Link to="/brands"> Brands</Link>
                  </Scroll>
                </Menu.Item>
                <Menu.Item name="ethical_code" active={this.state.activeItem === 'ethical_code'} onClick={this.handleItemClick}>
                  <Scroll type="id" element="ethicalcode" offset={-100}>
                    <Link to="/ethical_code"> Ethical Code</Link>
                  </Scroll>
                </Menu.Item>
                <Menu.Item name="investor_relations" active={this.state.activeItem === 'investor_relations'} onClick={this.handleItemClick}>
                  <Scroll type="id" element="investorrelations" offset={-100}>
                    <Link to="/investor_relations"> Investor Relations</Link>
                  </Scroll>
                </Menu.Item>
              </Menu>
            </Grid.Row>
          </Grid>
        </Menu>
      </div>
    );
  }
}
